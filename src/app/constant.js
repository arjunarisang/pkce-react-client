export const BASE_URL = process.env.REACT_APP_AUTH_BASE_URL;
export const CLIENT_ID = process.env.REACT_APP_AUTH_CLIENT_ID;
export const REDIRECT_URI = process.env.REACT_APP_AUTH_REDIRECT_URI;
export const SCOPE = "openid profile email address phone";
export const TOKEN_URL = `${BASE_URL}/v1/token`
