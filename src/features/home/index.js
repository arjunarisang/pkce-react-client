import React, { useEffect } from "react";
import { BASE_URL, CLIENT_ID, REDIRECT_URI, SCOPE } from "../../app/constant";
import { useSelector, useDispatch } from "react-redux";
import { generateChallenge, generateState } from "../callback/authSlice";

const Home = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(generateState());
    dispatch(generateChallenge());
  }, [dispatch]);

  const auth = useSelector((state) => state.auth);
  const loginUrl = `${BASE_URL}/v1/authorize?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=code&state=${auth.state}&code_challenge=${auth.challenge}&code_challenge_method=S256&scope=${SCOPE}`;

  return (
    <div>
      <p>Home</p>
      <a href={loginUrl}>Login</a>
      <p>Access Token: {auth.accessToken}</p>
      <p>Id Token: {auth.idToken}</p>
    </div>
  );
};

export default Home;
