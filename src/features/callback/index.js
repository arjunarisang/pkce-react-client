import React, { useEffect } from "react";

import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  generateChallenge,
  generateVerifier,
  getToken,
  resetToken,
} from "./authSlice";

const Callback = () => {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const history = useHistory();
  const params = new URLSearchParams(window.location.search);
  const receivedCode = params.get("code");

  useEffect(() => {
    dispatch(resetToken());
    if (receivedCode !== null) {
      dispatch(getToken(receivedCode));
    }
  }, [receivedCode, dispatch]);

  useEffect(() => {
    if (auth.accessToken !== "") {
      history.push("/");
    }
  }, [auth, history]);

  return (
    <div>
      <p>Callback</p>
      <div>
        <p>Hi, {auth.verifier}</p>
        <button onClick={() => dispatch(generateVerifier())}>
          generateVerifier
        </button>
      </div>
      <div>
        <p>Hi, {auth.challenge}</p>
        <button onClick={() => dispatch(generateChallenge())}>
          generateChallenge
        </button>
      </div>
    </div>
  );
};

export default Callback;
