import { createSlice } from "@reduxjs/toolkit";
import cryptoBrowserify from "crypto-browserify";
import Axios from "axios";
import { TOKEN_URL, CLIENT_ID, REDIRECT_URI } from "../../app/constant";

const base64URLEncode = (str) => {
  return str
    .toString("base64")
    .replace(/\+/g, "-")
    .replace(/\//g, "_")
    .replace(/=/g, "");
};

const sha256 = (buffer) => {
  return cryptoBrowserify.createHash("sha256").update(buffer).digest();
};

export const authSlice = createSlice({
  name: "auth",
  initialState: {
    value: 0,
    accessToken: "",
    refreshToken: "",
    idToken: "",
    verifier: base64URLEncode(cryptoBrowserify.randomBytes(32)),
    challenge: "",
    state: base64URLEncode(cryptoBrowserify.randomBytes(32)),
  },
  reducers: {
    generateVerifier: (state) => {
      state.verifier = base64URLEncode(cryptoBrowserify.randomBytes(32));
    },
    generateChallenge: (state) => {
      state.challenge = base64URLEncode(sha256(state.verifier));
    },
    generateState: (statez) => {
      statez.state = base64URLEncode(cryptoBrowserify.randomBytes(32));
    },
    receivedToken: (state, action) => {
      const res = action.payload;
      
      state.accessToken = res.access_token;
      state.idToken = res.id_token;
    },
    resetToken: (state) => {
      state.accessToken = "";
      state.idToken = "";
    },
  },
});

export const {
  generateChallenge,
  generateState,
  generateVerifier,
  receivedToken,
  resetToken,
} = authSlice.actions;

export const getToken = (receivedCode) => async (dispatch, getState) => {
  const { auth } = getState();
  const response = await Axios.post(TOKEN_URL, null, {
    params: {
      client_id: CLIENT_ID,
      code_verifier: auth.verifier,
      redirect_uri: REDIRECT_URI,
      grant_type: "authorization_code",
      code: receivedCode,
    },
  });

  dispatch(receivedToken(response.data));
};

export default authSlice.reducer;
